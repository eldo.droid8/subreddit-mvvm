package `in`.ev.subreddit.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SubRedditApp: Application()
