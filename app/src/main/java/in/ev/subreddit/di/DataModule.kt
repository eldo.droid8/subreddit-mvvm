package `in`.ev.subreddit.data.di.module

import `in`.ev.data.model.ErrorData
import `in`.ev.subreddit.data.api.SubRedditApi
import `in`.ev.subreddit.data.datasource.remote.SubRedditRemoteDataSource
import com.squareup.moshi.JsonAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object DataSourceModule {
    @Provides
    @Singleton
    fun provideDataSource(
        api: SubRedditApi, retrofit: Retrofit, moshiAdapter:
        JsonAdapter<ErrorData>
    ): SubRedditRemoteDataSource {
        return SubRedditRemoteDataSource(api, retrofit, moshiAdapter)
    }

}

/*@Module
@InstallIn(ApplicationComponent::class)
object RepoModule {
    @Provides
    @Singleton

    fun provideRepoImpl(
        dataSource: SubRedditRemoteDataSource
    ): SubRedditPostsRepository {
        return RemoteRepoImpl(dataSource)
    }

}*/
