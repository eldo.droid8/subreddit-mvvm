package `in`.ev.subreddit.data.datasource.remote

import `in`.ev.data.model.ErrorData
import `in`.ev.subreddit.data.model.NetworkResult
import com.squareup.moshi.JsonAdapter
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.net.SocketException

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
abstract class BaseDataSource constructor(
    val retrofit: Retrofit, private val moshiErrorDataAdapter:
    JsonAdapter<ErrorData>
) {
    protected suspend fun <T : Any> getResponse(
        request: suspend () -> Response<T>
    ): NetworkResult<T> {
        return try {
            val result = request.invoke()
            return if (result.isSuccessful) {
                   NetworkResult.ApiCallSuccess(result.body()!!)
            } else {
                val errorResponse: ErrorData = parseError(result)
                NetworkResult.ApiCallError(errorResponse)
            }
        } catch (e: IOException) {
            NetworkResult.ApiCallError(ErrorData(status_message = "Unknown error"))
        } catch (e: SocketException) {
            NetworkResult.ApiCallError(ErrorData(status_message = "Please check your network connection"))
        } catch (e: HttpException) {
            val errorResponse = convertErrorBody(e)
            NetworkResult.ApiCallError(errorResponse)
        } catch (e: KotlinNullPointerException) {
            NetworkResult.ApiCallError(ErrorData(status_message = "Empty response"))
        }

    }

    private fun parseError(response: Response<*>): ErrorData {
        val converter = retrofit.responseBodyConverter<ErrorData>(
            ErrorData::class.java, arrayOfNulls
                (0)
        )
        return try {
            val errorEntity = converter.convert(response.errorBody()) ?: ErrorData(
                status_message = "Unknown " + "error")
            errorEntity

        } catch (e: IOException) {
            ErrorData(status_message = "Io Exception")
        }
    }

    private fun convertErrorBody(throwable: HttpException): ErrorData {
        return try {
            val response = moshiErrorDataAdapter.fromJson(throwable.response()?.errorBody()?.source())
            response ?: ErrorData(status_message = "Unknown Error")
        } catch (exception: Exception) {
            val errorEntity = ErrorData(status_message = "Unknown error")
            errorEntity
        }
    }

}