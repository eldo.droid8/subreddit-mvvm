
package `in`.ev.subreddit.data.datasource.paging

import `in`.ev.subreddit.data.datasource.remote.SubRedditRemoteDataSource
import `in`.ev.subreddit.data.model.NetworkResult
import `in`.ev.subreddit.data.model.remote.SubRedditInfo
import androidx.paging.PagingSource
import androidx.paging.PagingSource.LoadResult.Page
import androidx.paging.PagingState

class SubredditPagingSource(
    private val subRedditRemoteDataSource: SubRedditRemoteDataSource
) : PagingSource<String, SubRedditInfo>() {
    override suspend fun load(params: LoadParams<String>): LoadResult<String, SubRedditInfo> {
        //return try {

        val response = subRedditRemoteDataSource.getSubRedditPosts(
            params.loadSize.toString(),
            after = params.key ?: "",
            before = params.key ?: ""
        )

        return when (response) {
                is NetworkResult.ApiCallSuccess -> {
                    val data = response.data?.data?.children
                        Page(
                            data = data.map { it.data } , prevKey = response.data?.data?.before,
                            nextKey = response.data?.data?.after
                        )
                    }
                is NetworkResult.ApiCallError -> {
                    LoadResult.Error(throwable = Throwable(response.error.status_message))
                }
                else -> {
                    LoadResult.Error(throwable = Throwable("Unknown Error"))
                }
        }
    }

    override fun getRefreshKey(state: PagingState<String, SubRedditInfo>): String? {
        return ""
    }

}
