package `in`.ev.subreddit.data.datasource.remote
import `in`.ev.data.model.ErrorData
import `in`.ev.subreddit.data.api.SubRedditApi
import `in`.ev.subreddit.data.model.NetworkResult
import `in`.ev.subreddit.data.model.remote.SubRedditEntity
import com.squareup.moshi.JsonAdapter
import retrofit2.Retrofit
import javax.inject.Inject

class SubRedditRemoteDataSource @Inject constructor(
    private val subRedditApi: SubRedditApi,
    retrofitClient: Retrofit, moshiAdapter: JsonAdapter<ErrorData>
) : BaseDataSource(
    retrofitClient,
    moshiAdapter
) {
    suspend fun getSubRedditPosts(limit: String,after: String = "", before: String = ""):
            NetworkResult<SubRedditEntity> {
        return getResponse(
            request = { subRedditApi.getSubReddits(limit, after, before)}
        )
    }

}