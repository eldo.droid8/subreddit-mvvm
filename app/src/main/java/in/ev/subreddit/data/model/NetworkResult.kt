package `in`.ev.subreddit.data.model

import `in`.ev.data.model.ErrorData

sealed  class NetworkResult<out T : Any> {
    object Loading: NetworkResult<Nothing>()
    data class ApiCallSuccess<out T : Any>(val data: T): NetworkResult<T>()
    data class ApiCallError(val error: ErrorData): NetworkResult<Nothing>()
}

