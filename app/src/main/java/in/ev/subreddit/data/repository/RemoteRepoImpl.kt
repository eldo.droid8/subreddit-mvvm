package `in`.ev.subreddit.data.repository

import `in`.ev.domain.repository.SubRedditRemoteRepository
import `in`.ev.subreddit.data.datasource.paging.SubredditPagingSource
import `in`.ev.subreddit.data.datasource.remote.SubRedditRemoteDataSource
import androidx.paging.Pager
import androidx.paging.PagingConfig
import javax.inject.Inject

class RemoteRepoImpl @Inject constructor(private val remoteDataSource: SubRedditRemoteDataSource):
    SubRedditRemoteRepository {
    override fun getSubRedditPosts(postId: String, pageSize: Int)= Pager(
        PagingConfig(pageSize)
    ) {
        SubredditPagingSource(
           subRedditRemoteDataSource = remoteDataSource
        )
    }.flow
}