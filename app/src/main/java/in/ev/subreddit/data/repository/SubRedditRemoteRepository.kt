package `in`.ev.domain.repository

import `in`.ev.subreddit.data.model.remote.SubRedditInfo
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow

interface SubRedditRemoteRepository {
    fun getSubRedditPosts(postId: String, pageSize: Int): Flow<PagingData<SubRedditInfo>>
}