package `in`.ev.data.model

data class ErrorData(val status_code: Int = 0,
                     val status_message: String? = null)